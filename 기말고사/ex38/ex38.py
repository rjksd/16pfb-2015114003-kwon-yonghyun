# -*-coding:utf8

ten_things = "Apples Oranges Crows Telephone Light Sugar"
# ten_things 변수에 6가지 값을 대입합니다.

print "Wait there are not 10 things in that list. Let's fix that."
# 출력

stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]
# more_stuff 변수에 8가지 추가적인 값을 추가합니다.

while len(stuff) != 10:
    next_one = more_stuff.pop()
    print "Adding: ", next_one
    stuff.append(next_one)
    print "There are %d items now." % len(stuff)
# more_stuff 의 끝 값부터 하나씩 stuff 에 추가합니다.

print "There we go: ", stuff
# 출력

print "Let's do some things with stuff."
# 출력

print stuff[1]
print stuff[-1]
print stuff.pop()
print ' '.join(stuff)
print '#'.join(stuff[3:5])