# -*-coding: utf8

people = 10
# people에 10 대입
cats = 30
# cats에 30 대입
dogs = 15
# dogs에 15 대입


if people < cats:
    print "Too many cats! The world is doomed!"
# cats값이 people보다 크면 아래 구문 출력

if people > cats:
    print "Not many cats! The world is saved!"
# cats값이 people보다 작으면 아래 구문 출력

if people < dogs:
    print "The world is drooled on!"
# dogs값이 people보다 크면 아래 구문 출력

if people > dogs:
    print "The world is dry!"
# dogs값이 people보다 작으면 아래 구문 출력


dogs += 5
# dogs값에 5 더하기 (dogs값이 20이 되었다.)

if people >= dogs:
    print "People are greater than or equal to dogs."
# people 값이 dogs보다 크거나 같으면 아래 구문 출력

if people <= dogs:
    print "People are less than or equal to dogs."
# dogs값이 people값보다 크거나 같으면 아래 구문 출력


if people == dogs:
    print "People are dogs."
# people값과 dogs값이 같으면 아래 구문 출력