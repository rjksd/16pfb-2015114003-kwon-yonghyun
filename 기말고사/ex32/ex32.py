# -*-coding:utf8

the_count = [1, 2, 3, 4, 5]
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies', 2, 'dimes', 3, 'quarters']
# 각 변수에 5개, 4개, 6개의 값을 저장합니다(순서를 가집니다.).

# this first kind of for-loop goes through a list
for number in the_count:
    print "This is count %d" % number
# number변수에 the_count 값을 반복합니다? , 출력

# same as above
for fruit in fruits:
    print "A fruit of type: %s" % fruit
# fruit 변수에 fruits 값을 반복합니다.?, 출력

# also we can go through mixed lists too
# notice we have to use %r since we don't know what's in it
for i in change:
    print "I got %r" % i
# i 변수에 change값을 반복합니다?, 출력

# we can also build lists, first start with an empty one
elements = []
# empty

# then use the range function to do 0 to 5 counts
for i in range(0, 6):
    print "Adding %d to the list." % i
    # append is a function that lists understand
    elements.append(i)
# i 변수 (change 값) 에서 0~6까지 반복합니다. append=?

# 또는 python 에서는 아래와 같이 할 수도 있다.
# list comprehension
elements2 = [i for i in range (0,6)]

# now we can print them out too
for i in elements:
    print "Element was: %d" % i
# i 변수에 elements 값을 반복합니다., 출력