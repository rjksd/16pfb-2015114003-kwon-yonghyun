# coding: utf-8

print"1. True and True = %s" %str(True and True)
#참 그리고 참은 참
print"2. False and True = %s" % str(False and True)
#거짓 그리고 참은 거짓
print"3. 1==1 and 2==1 = %s" % ((1==1) and (2==1))
#참 그리고 거짓은 거짓
print"4. 'test' == 'test' = %s" % str('test' == 'test')
#구문이 같으면 참
print"5. 1==1 or 2!=1 = %s" % (1==1) or(2 != 1)
#참 또는 거짓은 참
print"6. True and 1==1 = %s" %str(True and (1==1))
#참 그리고 참은 참
print"7. False and 0!=0 = %s" %str(False and (0 !=0))
#거짓 그리고 거짓은 거짓
print"8. True or 1==1 = %s" % True or (1==1)
#참 또는 참은 참
print"9. 'test' == 'testing' = %s" % str('test' == 'testing')
#구문이 다르면 거짓
print"10. 1!=0 and 2==1 = %s" %str((1 !=0) and (2==1))
#거짓 그리고 거짓은 거짓
print"11. 'test'!='testing' = %s" %str('test'!= 'testing')
#?
print"12. 'test'==1 = %s" % str('test'==1)
# 구문=숫자->거짓
print"13. not(True and False) = %s" %(not(True and False))
#(참 그리고 거짓)이 아니다->참
print"14. not(1==1 and 0!=1) = %s" %(not(1==1)and (0!=1))
#(참 그리고 참)이 아니다->거짓
print"15. not(10==1 or 1000==1000 = %s" % (not (10==1) or (1000==1000))
#(거짓 또는 참)이 아니다->참
print"16. not(1!=10 or 3==4) = %s" % (not (1 != 10) or (3==4))
#(거짓 또는 거짓)이 아니다->참
print"17. not('testing'=='testing' and 'Zed'=='Cool Guy') = %s" % (not ('testing'=='testing') and ('Zed'=='Cool Guy'))
#구문에서(참 그리고 거짓)이 아니다->거짓??????
print"18. 1==1 and not('testing'==1 or 1==0) = %s" %((1==1)and not ('testing' == 1) or (1==0))
#참 그리고 (거짓 또는 거짓)이 아니다 -> 참
print"19. 'chunky'=='bacon'and not (3==4 or 3==3) = %s" %(('chunky'=='bacon')and not ((3==4) or (3==3)))
#거짓 그리고 (거짓 또는 참)이 아니다 ->거짓
print"20. 3==3 and not ('testing'=='testing' or 'Python'=='Fun') = %s" %((3==3)and not (('testing' == 'testing')or('Python' == 'Fun')))
#참 그리고 (참 또는 거짓)이 아니다 -> 거짓

print"extra 1. 'test' and 'test' = = %s" % str('test' and 'test')
#test 그리고 test는 test
print"extra 2. 1 and 1 = = %s" %str(1 and 1)
#1그리고 1은 1
print"extra 3. False and 1 = = %s" % str(False and 1)
#거짓 그리고 1은 거짓
print"extra 4. True and 1 = = %s" % str(True and 1)
#참 그리고 1은 1