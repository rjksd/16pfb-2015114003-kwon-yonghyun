# -*- coding:utf8

animals = ['곰', '호랑이', '펭귄', '얼룩말']
bear = animals[0]
tiger = animals[1]
penguin = animals[2]
zebra = animals[3]
# animals 변수에 4가지 값을 대입합니다.(순서를 가집니다.) 그리고 bear 변수에 animal 변수의 첫번째 값을, tiger에 두번째 값, penguin에 세번째 값, zebra에 네번쨰 값을 대입합니다.

print "bear = %s" % bear
print "tiger = %s" % tiger
print "penguin = %s" % penguin
print "zebra = %s" % zebra
# 출력합니다.