# -*-coding: utf8

people = 30
# people변수에 30 대입
cars = 40
# cars변수에 40 대입
trucks = 15
# trucks변수에 15 대입


if cars > people:
    print "We should take the cars."
# cars>people일 때 아래 구문 출력
elif cars < people:
    print "We should not take the cars."
# cars<people일 때 아래 구문 출력
else:
    print "We can't decide."
# 둘 다 아닐 때(cars=people일 때) 아래 구문 출력

if trucks > cars:
    print "That's too many trucks."
# trucks > cars일 때 아래 구문 출력
elif trucks < cars:
    print "Maybe we could take the trucks."
# trucks < cars일 때 아래 구문 출력
else:
    print "We still can't decide."
# 둘 다 아닐 떄(trucks = cars일 때) 아래 구문 출력

if people > trucks:
    print "Alright, let's just take the trucks."
# people > trucks일 때 아래 구문 출력
else:
    print "Fine, let's stay home then."
# 아닐 떄(people <= trucks일 때) 아래 구문 출력