# -*-coding:utf8

print "You enter a dark room with two doors.  Do you go through door #1 or door #2?"
# 문장 출력

door = raw_input("> ")
# door 값을 받습니다.

if door == "1":
    print "There's a giant bear here eating a cheese cake.  What do you do?"
    print "1. Take the cake."
    print "2. Scream at the bear."
# 받은 door 값이 1일 때 다음 3문장을 출력합니다.

    bear = raw_input("> ")
# bear 값을 받습니다.

    if bear == "1":
        print "The bear eats your face off.  Good job!"
    elif bear == "2":
        print "The bear eats your legs off.  Good job!"
    else:
        print "Well, doing %s is probably better.  Bear runs away." % bear
# 받은 bear 값이 1일 땐 첫번쨰 문장, 2일 떈 두번째 문장, 두개 다 아닐 떄 세번째 문장을 출력합니다.

elif door == "2":
    print "You stare into the endless abyss at Cthulhu's retina."
    print "1. Blueberries."
    print "2. Yellow jacket clothespins."
    print "3. Understanding revolvers yelling melodies."
# 받은 door값이 2일 때 다음 4문장을 출력합니다.

    insanity = raw_input("> ")
# insanity값을 받습니다.

    if insanity == "1" or insanity == "2":
        print "Your body survives powered by a mind of jello.  Good job!"
    else:
        print "The insanity rots your eyes into a pool of muck.  Good job!"
# 받은 insanity값이 1 또는 2일 때 첫번째 문장을, 둘 다 아닐 때 두번쨰 문장을 출력합니다.

else:
    print "You stumble around and fall on a knife and die.  Good job!"
# 받은 door 값이 1 또는 2가 아닐 때 다음 문장을 출력합니다.