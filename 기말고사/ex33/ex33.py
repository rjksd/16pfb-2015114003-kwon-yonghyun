# -*-coding:utf8

i = 0
numbers = []
# i 변수에 0을 대입합니다. numbers 변수는 비어있습니다.

while i < 6:
    print "At the top i is %d" % i
    numbers.append(i)
# i 의 값이 6보다 작을 떄 아래 문장을 출력합니다. numbers 변수에 i를 추가합니다.

    i = i + 1
    print "Numbers now: ", numbers
    print "At the bottom i is %d" % i
# i 변수에 i+1을 대입합니다.


print "The numbers: "

for num in numbers:
    print num
# num 변수에 numbers 값을 대입합니다., 출력